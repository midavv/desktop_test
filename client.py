import tkinter as tk
from tkinter import ttk
import pandas as pd
import requests
import json


resp = requests.get('http://127.0.0.1:5000/attractionstype')
if resp.status_code == 200:
	attraction_list = json.loads(resp.text)
else:
	attraction_list = ['Other']

def send_data(event):
	print('tadaa!')
	data = {
	    'Charge':'Paid' if comboCharge.get()=='Нет' else 'Free',
	    'Visitors':int(ent_visitors.get()),
	    'AttractionType':comboType.get()
    }
	resp = requests.post('http://127.0.0.1:5000/attraction', data=data)
	recomendation['text'] = 'Результат: ' + '\n'.join(json.loads(resp.text))

 
# Создается новое окно с заголовком "Введите домашний адрес"
window = tk.Tk()
window.title("Заполните анкету")
 
# Создается новая рамка `frm_form` для ярлыков с текстом и
# Однострочных полей для ввода информации об адресе.
frm_form = tk.Frame(relief=tk.SUNKEN, borderwidth=1)
# Помещает рамку в окно приложения.
frm_form.pack()
 
# Создает ярлык и текстовок поле для ввода имени.
lbl_first_name = tk.Label(master=frm_form, text="Бесплатная зарядка")
comboCharge = ttk.Combobox(frm_form, 
                            values=[
                                    "Да", 
                                    "Нет"])
# Использует менеджер геометрии grid для размещения ярлыка и
# однострочного поля для ввода текста в первый и второй столбец
# первой строки сетки.
lbl_first_name.grid(row=0, column=0, sticky="e")
comboCharge.grid(row=0, column=1)
 
# Создает ярлык и текстовок поле для ввода фамилии.
lbl_last_name = tk.Label(master=frm_form, text="Количество посетителей:")
ent_visitors = tk.Entry(master=frm_form, width=50)
# Размещает виджеты на вторую строку сетки
lbl_last_name.grid(row=1, column=0, sticky="e")
ent_visitors.grid(row=1, column=1)
 
# Создает ярлык и текстовок поле для ввода первого адреса.
lbl_address1 = tk.Label(master=frm_form, text="Тип:")
comboType = ttk.Combobox(frm_form, 
                            values=attraction_list)
# Размещает виджеты на третьей строке сетки.
lbl_address1.grid(row=2, column=0, sticky="e")
comboType.grid(row=2, column=1, sticky=tk.W)
 
# Создает ярлык и текстовок поле для ввода второго адреса.
recomendation = tk.Label(master=frm_form, text="Результат:")
# ent_address2 = tk.Entry(master=frm_form, width=50)
# Размещает виджеты на четвертой строке сетки.
recomendation.grid(row=3,  sticky=tk.E)
# ent_address2.grid(row=3, column=1)
 
# Создает новую рамку `frm_buttons` для размещения
# кнопок "Отправить" и "Очистить". Данная рамка заполняет
# все окно в горизонтальном направлении с
# отступами в 5 пикселей горизонтально и вертикально.
frm_buttons = tk.Frame()
frm_buttons.pack(fill=tk.X, ipadx=5, ipady=5)
 
# Создает кнопку "Отправить" и размещает ее
# справа от рамки `frm_buttons`.
btn_submit = tk.Button(master=frm_buttons, text="Отправить")
btn_submit.pack(side=tk.RIGHT, padx=10, ipadx=10)
 
# Создает кнопку "Очистить" и размещает ее
# справа от рамки `frm_buttons`.
btn_clear = tk.Button(master=frm_buttons, text="Очистить")
btn_clear.pack(side=tk.RIGHT, ipadx=10)
 
btn_submit.bind('<Button-1>', send_data)
# Запуск приложения.
window.mainloop()